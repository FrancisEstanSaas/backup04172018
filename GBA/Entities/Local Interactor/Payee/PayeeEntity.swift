//
//  PayeeEntity.swift
//  GBA
//
//  Created by Republisys on 22/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

struct PayeeEntity: Decodable{
    var uid: Int
    var nickname: String
    var firstname: String
    var lastname: String
    var mobile: String
    var email: String
    var image_id: String? = ""
    var created_at: String
    var status: String
    var currency: String? = ""
    var country_id: String
    
}
