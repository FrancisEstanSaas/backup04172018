//
//  DashboardLocalInteractor.swift
//  GBA
//
//  Created by Republisys on 24/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class DashboardLocalInteractor: RootLocalInteractor{
    var wallet:Wallet?{ get{ return GBARealm.objects(Wallet.self).first } }
    
    var userProfile: User?{ get { return GBARealm.objects(User.self).first } }
}
