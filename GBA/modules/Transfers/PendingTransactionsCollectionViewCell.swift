//
//  PendingTransactionsCollectionViewCell.swift
//  GBA
//
//  Created by Gladys Prado on 1/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class PendingTransactionsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblRecipientName: UILabel!
    @IBOutlet weak var lblTransferAmount: UILabel!
    
}
