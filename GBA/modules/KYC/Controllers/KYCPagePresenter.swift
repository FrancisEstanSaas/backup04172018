//
//  KYCPagePresenter.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

protocol DataReceivedFromKYC{
    func didReceiveResponse(code: String)
}

class KYCPagePresenter: KYCRootPresenter {
    
    var customerEntity: KYCInfoEntity? = nil
    var dataToBridgeKYC: DataReceivedFromKYC? = nil
    var didEditForm: Bool? = false
    
    //check if needDataHolder as Entity is already set
    
    func submitKYCInfoDetails(form: KYCInfoEntity) {
        
        print(form)
        self.customerEntity = form
        guard let bridge = dataToBridgeKYC else { fatalError("dataBridge was not implemented in RegistrationPresenter") }
        
        
    }
    
}
