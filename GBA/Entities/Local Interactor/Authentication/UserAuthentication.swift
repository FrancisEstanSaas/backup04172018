//
//  UserAuthentication.swift
//  GBA
//
//  Created by Emmanuel Albania on 15/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import RealmSwift

class UserAuthentication: Object{
    @objc dynamic var expiry        : Int       = 0
    @objc dynamic var accessToken   : String    = ""
    @objc dynamic var refreshToken  : String    = ""
    @objc dynamic var authLevel     : Int       = 1
    @objc dynamic var tokenType     : String    = ""
    @objc dynamic var deviceType    : Int       = 0
    
    convenience init(json: JSON) {
        self.init()
        //(rawValue: (json["device_type"] as? Int ?? 0))
        guard let validity = json["expires_in"] as? Int,
            let access_token = json["access_token"] as? String,
            let refresh_token = json["refresh_token"] as? String,
            let auth_level = GBAAuth.AuthenticationLevel(rawValue: (json["auth_level"] as? Int)!),
            let token_type = GBAAuth.TokenType(rawValue: (json["token_type"] as? String)!),
            let device_type = GBAAuth.DeviceType(rawValue: (json["device_type"] as? Int)!)
        else{
            fatalError("Parsing Authentication encountered an error in UserAuthentication")
        }
        
        self.expiry = validity
        self.accessToken = access_token
        self.refreshToken = refresh_token
        self.authLevel = auth_level.rawValue
        self.tokenType = token_type.rawValue
        self.deviceType = device_type.rawValue
        
    }
    
    override static func primaryKey()->String?{
        return "accessToken"
    }
}

extension UserAuthentication{
    var getGBAAuthLevel: GBAAuth.AuthenticationLevel{
        return GBAAuth.AuthenticationLevel(rawValue: self.authLevel)!
    }
    
    var getGBAAuthTokenType: GBAAuth.TokenType{
        return GBAAuth.TokenType(rawValue: self.tokenType)!
    }
    
    var getGBAAuthDeviceType: GBAAuth.DeviceType{
        return GBAAuth.DeviceType(rawValue: self.deviceType)!
    }
}

extension UserAuthentication: LocalEntityProcess{
    
}
