//
//  GBAVerificationLabelView.swift
//  GBA
//
//  Created by Republisys on 21/02/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class GBAVerificationLabelView: UIView{
    
    private var underline: UIView!
    private var characterLabel: UILabel!
    
    private var character: Character? = nil{
        didSet{
            guard let char = self.character else {
                self.characterLabel = nil
                return
            }
            
            self.subviews.forEach{ $0 == self.underline ? () : $0.removeFromSuperview() }
            
            characterLabel = UILabel()
            characterLabel.translatesAutoresizingMaskIntoConstraints = false
            characterLabel.textColor = .gray
            characterLabel.text = String(char)
            characterLabel.font = UIFont.fontAwesome(ofSize: self.frame.height * 0.7)
            characterLabel.textAlignment = .center
            
            self.addSubview(characterLabel)

            characterLabel.topAnchor.constraint(equalTo: self.topAnchor).Enable()
            characterLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
            characterLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
            characterLabel.bottomAnchor.constraint(equalTo: self.underline.topAnchor, constant: self.frame.height * -0.1).Enable()
            
            self.layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.underline = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 10, height: 10)))
        
        self.underline.backgroundColor = .gray
        self.underline.translatesAutoresizingMaskIntoConstraints = false
        self.underline.layer.cornerRadius = self.frame.height * 0.1
        
        self.addSubview(underline)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func layoutUnderline(){
        
        _ = self.underline.constraints.map{ $0.Disable() }
        self.underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: self.frame.height * 0.1).Enable()
        self.underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: self.frame.height * -0.05).Enable()
        self.underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: self.frame.height * -0.1).Enable()
        self.underline.heightAnchor.constraint(equalToConstant: self.frame.height * 0.05).Enable()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutUnderline()
    }
}

extension GBAVerificationLabelView{
    @discardableResult
    func set(char: Character)->Self{
        self.character = char
        return self
    }
}

