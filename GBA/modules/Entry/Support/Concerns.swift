//
//  Concerns.swift
//  GBA
//
//  Created by EDI on 18/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

enum Concerns {
    
    case Dashboard
    case AccountSettings
    case Devices
    case FAQs
    case ReloadWallet
    case Others
    
    var name: String{
        switch self{
        case .Dashboard: return "Dashboard"
        case .AccountSettings: return "Account Settings"
        case .Devices: return "Devices"
        case .FAQs: return "FAQs"
        case .ReloadWallet: return "Reload Wallet"
        case .Others: return "Others"
        }
    }
    
    var type: String{
        switch self{
        case .Dashboard: return "1"
        case .AccountSettings: return "2"
        case .Devices: return "3"
        case .FAQs: return "4"
        case .ReloadWallet: return "5"
        case .Others: return "6"
        }
    }
    
}


 
