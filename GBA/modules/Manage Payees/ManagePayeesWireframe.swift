
//
//  ManagePayeesWireframe.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class ManagePayeesWireframe: RootWireframe{
    
    
    func configureModule(activity: ManagePayeesActivities){
        guard let target =  self._target else{
            fatalError("[ManagePayeesWireframe: configureModule] target not found")
        }
        
        if _presenter == nil{
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! ManagePayeesModuleViewController, and: self) as? ManagePayeesRootPresenter else{
                fatalError("declared presenter cannot be found in ManagePayees module")
            }
            
            self.set(presenter: presenter)
        }else{
            guard let presenter = _presenter as? ManagePayeesRootPresenter else{
                fatalError("declared presenter cannot be parsed for ManagePayees module")
            }
            
            presenter.view = target.module.activity!.viewController as! ManagePayeesModuleViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: ManagePayeesActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .ManagePayees(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension ManagePayeesWireframe{
    
    func navigate(to activity: ManagePayeesActivities, with presenter: ManagePayeesRootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .ManagePayeeListscreen:
            transition = .root
        case .AddRecipientscreen:
            transition = .push
        case .NewPayeescreen:
            transition = .push
            if presenter == nil { fatalError("no presenter found to initialize in newPayeescreen") }
        case .PayeeProfilescreen:
            transition = .push
        case .AddNewRecipientscreen:
            transition = .push
        case .EditPayeescreen(_):
            transition = .push
            
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: ManagePayeesActivities)->ManagePayeesModuleViewController?{
        self.set(target: .ManagePayees(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? ManagePayeesModuleViewController,
            let presenter = self._presenter else { return nil}
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
    
}

extension ManagePayeesWireframe{
    
    func presentCodeVerificationViewController(from sender: GBAVerificationCodeDelegate){
        self.navigator.pushViewController(GBACodeVerificationViewController().set(delegate: sender),
                                          animated: true)
    }
    
    func presentSuccessPage(title: String, message: NSAttributedString){
        let vc = GBASuccessPage()
            .set(title: title)
            .set(message: message)
        
        self.navigator.pushViewController(vc, animated: true)
    }
}
