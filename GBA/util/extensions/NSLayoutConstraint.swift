



import UIKit

extension NSLayoutConstraint{
    @discardableResult
    func Enable()->Self{
        self.isActive = true
        return self
    }
    
    @discardableResult
    func Disable()->Self{
        self.isActive = false
        return self
    }
    
    func final(){
        
    }
}


