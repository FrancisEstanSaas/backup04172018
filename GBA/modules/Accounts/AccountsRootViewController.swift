//
//  AccountsRootViewController.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class AccountsRootViewController: RootViewController {
    
    var presenter: AccountsRootPresenter {
        get{
            let accountsPresenter = self._presenter as! AccountsRootPresenter
            return accountsPresenter
        }
    }
    
}
