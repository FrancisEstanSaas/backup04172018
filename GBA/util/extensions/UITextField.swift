



import UIKit.UITextField

extension UITextField{
    open override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.autocorrectionType = .no
        self.textContentType = .name
    }
    
    @discardableResult
    func set(font style: UIFont)->Self{
        self.font = style
        return self
    }
    
    @discardableResult
    func set(value: String)->Self{
        self.text = value
        return self
    }
    
    @discardableResult
    func set(fontColor: UIColor)->Self{
        self.textColor = fontColor
        return self
    }
    
    @discardableResult
    func set(backgroundColor: UIColor)->Self{
        self.backgroundColor = backgroundColor
        return self
    }
}

