//
//  TransfersEntity.swift
//  GBA
//
//  Created by Gladys Prado on 24/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

struct TransferFormEntity: Decodable{
    var recipient_id: Int = 0
    var amount: Double = 0.00
    var date: String? = ""
    
    init(recipient_id: Int, amount: Double, date: String){
        self.recipient_id = recipient_id
        self.amount = amount
        self.date = date
    }
}
