//
//  AppDelegate.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/20/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import RealmSwift
import Firebase
     
var GBARealm = try! Realm()
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?

    let googleMapsApiKey = "AIzaSyAtS-fYvO7ywvXDpW92h4luoIS-sqkA_sY"
    //"AIzaSyAKF_fZmL8QQFIjpuELxAsuqbJd7ChME48"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        GMSServices.provideAPIKey(googleMapsApiKey)
        GMSPlacesClient.provideAPIKey(googleMapsApiKey)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let appNavigator = UINavigationController(rootViewController: Launchscreen())
//        let appNavigator = GBANavigationController(rootViewController: Launchscreen())


        window?.rootViewController = appNavigator
        window?.makeKeyAndVisible()
        
        //set navigation bar style
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
        
        //tab bar style
        UITabBar.appearance().tintColor = GBAColor.black.rawValue
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        self.truncateLocalDatabase()
        return true
    }
    
}

extension AppDelegate{
    fileprivate func truncateLocalDatabase(){
        do{
            try UserAuthentication().truncate()
            try User().truncate()
            try Payee().truncate()
            try Wallet().truncate()
            //try RegisteredUser().truncate()
            //try UserKeyInfo().truncate()
        }catch{ print(error.localizedDescription) }
        
    }
}

