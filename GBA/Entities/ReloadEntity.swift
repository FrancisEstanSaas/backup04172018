//
//  ReloadEntity.swift
//  GBA
//
//  Created by Gladys Prado on 9/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

struct DirectDepositEntity: Decodable{
    var account_name: String? = ""
    var account_number: String? = ""
    var aba_number: String? = ""
    var amount: String? = ""
    var wallet_id: String? = ""
    
    init(wallet_id: String, amount: String, account_name: String, account_number: String, aba_number: String ){
        self.wallet_id = wallet_id
        self.amount = amount
        self.account_name = account_name
        self.account_number = account_number
        self.aba_number = aba_number
    }
}

struct CreditCardReloadEntity: Decodable{
    var account_name: String? = ""
    var account_number: String = ""
    var cvv_number: String? = ""
    var card_expiry: String? = ""
    var amount: String? = ""
    var wallet_id: String = ""
    
    init(wallet_id: String, amount: String, account_name: String, account_number: String, card_expiry: String, cvv_number: String ){
        self.wallet_id = wallet_id
        self.amount = amount
        self.account_name = account_name
        self.account_number = account_number
        self.cvv_number = cvv_number
        self.card_expiry = card_expiry
    }
}
