



import UIKit

extension Double{
    var Float: CGFloat{ get{ return CGFloat(self) } }
    
    var toString: String{ get{ return String(describing: self) } }
}
