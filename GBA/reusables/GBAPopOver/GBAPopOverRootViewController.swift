//
//  GBAPopOverRootViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/19/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class GBAPopOverRootViewController: UITableViewController{
    
    var delegate: GBAPopOverDelegate? = nil
    
    @discardableResult
    func set(delegate: GBAPopOverDelegate)->Self{
        self.delegate = delegate
        return self
    }
}
