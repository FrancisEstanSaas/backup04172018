//
//  More_optionView.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/28/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class More_optionView: UIView{
    fileprivate var icon_image: UIImageView!
    fileprivate var title_label: UILabel!
    fileprivate var caret_image: UIImageView = UIImageView(image: UIImage.fontAwesomeIcon(name: .angleRight,
                                                                                          textColor: GBAColor.gray.rawValue,
                                                                                          size: CGSize(width: 40,
                                                                                                       height: 40)))
    
    fileprivate(set) var image: UIImage?{
        didSet{
            guard let _image = image else {
                self.caret_image = UIImageView()
                return
            }
            self.icon_image.image = _image
            
        }
    }
    
    fileprivate(set) var title: String?{
        didSet{
            guard let _title = self.title else{
                self.title_label.text = ""
                return
            }
            self.title_label.text = _title
        }
    }
    
    private var action: (()->Void)? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    private func xibSetup(){
        self.backgroundColor = .clear
        
        let underline = UIView()
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.backgroundColor = GBAColor.lightGray.rawValue
        
        self.icon_image = UIImageView()
        self.icon_image.translatesAutoresizingMaskIntoConstraints = false
        
        self.title_label = UILabel()
            .set(fontStyle: GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue)
            .set(color: GBAColor.darkGray.rawValue)
            .set(value: self.title ?? "")
            .add(to: self)
        
        self.caret_image.contentMode = .scaleAspectFit
        self.caret_image.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.icon_image)
        self.addSubview(self.caret_image)
        self.addSubview(underline)
        
        self.icon_image.topAnchor.constraint(equalTo: self.topAnchor, constant: 7).Enable()
        self.icon_image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).Enable()
        self.icon_image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -9).Enable()
        self.icon_image.widthAnchor.constraint(equalTo: self.icon_image.heightAnchor).Enable()
        
        self.caret_image.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).Enable()
        self.caret_image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).Enable()
        self.caret_image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -7).Enable()
        self.caret_image.widthAnchor.constraint(equalTo: self.caret_image.heightAnchor).Enable()

        self.title_label.topAnchor.constraint(equalTo: self.topAnchor).Enable()
        self.title_label.leadingAnchor.constraint(equalTo: self.icon_image.trailingAnchor, constant: 10).Enable()
        self.title_label.bottomAnchor.constraint(equalTo: self.bottomAnchor).Enable()
        self.title_label.trailingAnchor.constraint(equalTo: self.caret_image.leadingAnchor).Enable()
        
        underline.leadingAnchor.constraint(equalTo: self.title_label.leadingAnchor).Enable()
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1).Enable()
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        underline.heightAnchor.constraint(equalToConstant: 1).Enable()
        
    }
    
    @objc fileprivate func performAction(){
        if self.action != nil{
            (self.action)!()
        }
        
    }
}

extension More_optionView{
    @discardableResult
    func set(icon image: UIImage)->Self{
        self.image = image
        return self
    }
    
    @discardableResult
    func set(title name: String)->Self{
        self.title = name
        return self
    }
    
    @discardableResult
    func setAction(target: RootViewController, _ action: @escaping (()->Void))->Self{
        self.action = action
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(performAction)))
        return self
    }
}
