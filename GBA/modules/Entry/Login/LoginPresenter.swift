//
//  LoginPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/14/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol DataDidRecievedFromLogin{
    func didRecieveVeriftericationData(code: String)
}

class LoginPresenter: EntryRootPresenter{
    
    var dataBridgeToView: DataDidRecievedFromLogin? = nil
    var submittedForm: LoginFormEntity? = nil
    var countDownTimer: Int{
        get{ return UserDefaults.standard.integer(forKey: "Countdown") }
        set{ UserDefaults.standard.set(newValue < 0 ? 59: newValue, forKey: "Countdown") }
    }
    private var tryCount = 0
    
    
    func processLogin(form: LoginFormEntity, controller: LoginViewController){
        guard let nav = controller.navigationController else { fatalError("NavigationViewController can't properly parsed in LoginPresenter")}
        guard let bridge = dataBridgeToView else { fatalError("dataBridge was not implemented in LoginPresenter") }
        
        self.submittedForm = form
        
        self.interactor.remote.Login(form: form) { (reply, statusCode) in
            print(reply)
            switch statusCode{
            case .fetchSuccess:
                guard let verifiedStatus = reply["verified"] as? Bool else{
                    return
                }
                
                if verifiedStatus{
                    
                    let authData = UserAuthentication(json: reply)
                    authData.rewrite()
                    
                    switch authData.getGBAAuthLevel{
                    case .oneWay:
                        
                        self.fetchUserProfile(successHandler: { (reply, replyCode) in
                            DashboardWireframe(nav).presentTabBarController()
                        })
                        
                    case .twoWay:
                        self.wireframe.presentCodeVerificationViewController(
                            from: self.view as! GBAVerificationCodeDelegate,
                            completion: {
                                DashboardWireframe(nav).presentTabBarController()
                        }, apiCalls: { (code, vc) in
                            print(code)
                            self.interactor.remote.SubmitLoginVerificationCodes(code: code, accessToken: "String", successHandler: { (reply, replyCode) in
                                print(reply)
                                print(replyCode.rawValue)
                                print(replyCode)
                                
                                switch replyCode{
                                case .forbidden:
                                    self.showAlert(with: "Message", message: "Incorrect or expired verification code", completion: { print(reply) })
                                    
                                case .fetchSuccess:
                                    DashboardWireframe(nav).presentTabBarController()
                                    
                                default:
                                    print(reply)
                                }
                            })
                        }, backAction: {
                            self.view.navigationController?.popToRootViewController(animated: true)
                        })
                    }
                }else{
                    UserAuthentication(json: reply).rewrite()
                    
                    self.wireframe.presentCodeVerificationViewController(from: self.view as! LoginViewController,
                                                                         completion:{ ( ) },
                     apiCalls: { (code, view) in
                        print(code)
                        self.interactor.remote.SubmitRegistrationVerificationCode(code: code, to: form.mobile, successHandler: { (reply, replyCode) in
                            
                            print(replyCode)
                            
                            switch replyCode{
                            case .badRequest:
                                if let message = reply["message"] as? [String: Any],
                                    let code = (message["code"] as? [String])?.first{
                                    self.showAlert(with: "Message", message: code, completion: { print(reply) })
                                }
                                view.clearPIN()
                            case .fetchSuccess:
                                DashboardWireframe(nav).presentTabBarController()
                            default:
                                print(reply)
                            }
                        })
                    }, backAction: { self.view.navigationController?.popToRootViewController(animated: true) } )
                }
            case .badRequest:
                guard let messages = reply["message"] as? [String:Any] else{
                    fatalError("Message not found")
                }
                
                var message: String?
                
                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                
                self.showAlert(with: "Required fields empty", message: message ?? "message not found", completion: { () } )
            case .unauthorized:
                guard let message = reply["message"] as? String else{
                    fatalError("Message not found")
                }
                
                self.showAlert(with: "Notice", message: message, completion: { } )

                self.incementTryCount()
                
                
            default: break
            }
            bridge.didRecieveVeriftericationData(code: String(describing: reply))
        }
    }
    
    func resendVerificationCode(callback: @escaping ()->()){
        self.interactor.remote.ResendLoginVerificationCode { (reply, statusCode) in
            switch statusCode{
            case .accepted, .fetchSuccess:
                callback()
                self.showAlert(with: "Notice", message: "Please check your mobile for verification", completion: { } )
            default: return //break //@registration
            }
        }
    }
    
    func fetchUserProfile(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        self.interactor.remote.fetchUserProfile { (reply, replyCode) in
            print(reply)
            
            switch replyCode{
            case .fetchSuccess:
                User(json: reply).rewrite()
                successHandler(reply, replyCode)
            default: break
            }
            
        }
    }
    
    func fetchWallet(successHandler: @escaping (()->Void)){
        self.interactor.remote.fetchWallet { (reply, replyCode) in
            print(reply)
            
            switch replyCode{
            case .fetchSuccess:
                successHandler()
            default: break
            }
        }
    }
    
    func checkTryCounter(){
        resetCounter()
    }
    
    fileprivate func incementTryCount(){
        if self.tryCount >= 5{
            self.resetCounter()
            
        }
        else{ self.tryCount++ }
    }
    
    fileprivate func resetCounter(){
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.countDownTimer--
            (self.view as? LoginViewController)?.displayCountdown(timer: self.countDownTimer)
            if self.countDownTimer <= 0{
                (self.view as? LoginViewController)?.displayCountdown(timer: 0)
                self.tryCount = 0
                timer .invalidate()
            }
        })
    }
}
