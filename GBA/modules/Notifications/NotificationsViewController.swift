//
//  NotificationsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class NotificationsViewController: RootViewController{
    var presenter: NotificationsRootPresenter{
        get{
            let prsntr = self._presenter as! NotificationsRootPresenter
            return prsntr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
