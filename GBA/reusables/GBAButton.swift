//
//  GBAButton.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 11/6/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit

class GBAButton: UIButton{
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layoutButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutButton()
    }
    
    func layoutButton(){
        
        self.backgroundColor = GBAColor.primaryBlueGreen.rawValue
        self.setTitleColor(.white, for: .normal)
        self.layer.cornerRadius = 5
    }
}
