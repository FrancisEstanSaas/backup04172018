//
//  AccessSettingsEntity.swift
//  GBA
//
//  Created by EDI on 6/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

enum VerificationType {
    case OneWay
    case TwoWay
    
    var type: String{
        switch self{
        case .OneWay: return "1"
        case .TwoWay: return "2"
        }
    }
}

enum LogInPreference {
    case Password
    case PinCode
    case TouchId
    
    var type: String{
        switch self{
        case .Password: return "0"
        case .PinCode: return "1"
        case .TouchId: return "2"
        }
    }
}

enum LockGenerator {
    
    case key
    case iv
    
    var value: String{
        switch self {
        case .key: return "OaUhIQH2DA7x5A5FEk7A6J6AKFApfnwa" //"bbC2H19lkVbQDfakxcrtNMQdd0FloLyw" // length == 32
        case .iv: return "Ke58jahPjA3gtErQ" //"gqLOHUioQ0QjhuvI" // length == 16
        }
    }
    
}

enum SettingsMessages {
    case newUser
    case oldUser
    
    var title: String{
        switch self{
        case .newUser: return "Set a Security PIN"
        case .oldUser: return "Security PIN Required"
        }
    }
    
    var messages: String{
        switch self{
        case .newUser: return "To provide you better security, we will require you to set a Security PIN before changing your Settings."
        case .oldUser: return "To provide you better security, we will require you to set a 6-digit PIN before changing your Settings."
        }
    }
    
    
}

