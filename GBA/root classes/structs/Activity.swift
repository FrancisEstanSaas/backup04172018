//
//  Ac.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/25/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

struct Activity<View:RootViewController> {
    let identifier: String
    var viewController: View
    
    init(identifier: ViewControllerIdentifier, `in` module: Module){
        self.identifier = identifier
        
        guard let view = module.board.instantiateViewController(withIdentifier: identifier) as? View
            else{ fatalError("Storyboard and ViewController in activity struct doesn't match") }
        
        self.viewController = view
    }
    
}

