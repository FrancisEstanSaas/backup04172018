//
//  SettingsActivities.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

enum SettingsActivities: ViewControllerIdentifier {
    case SettingsMenuView = "SettingsMenuView"
    case MyCardView = "MyCardView"
    case AccessSettingsMainView = "AccessSettingsMainView"
    case UpdatePasswordView = "UpdatePasswordView"
    case ProfileView = "ProfileView"
    case EditProfileView = "EditProfileView"
    
    func getPresenter(with viewController: SettingsRootViewController, and wireframe: SettingsWireframe)->RootPresenter?{
        
        switch self {        case .SettingsMenuView:
            return SettingsMenuPresenter(wireframe: wireframe, view: viewController)
        case .MyCardView:            return MyCardPresenter(wireframe: wireframe, view: viewController)
        case .AccessSettingsMainView:
            return AccessSettingsPresenter(wireframe: wireframe, view: viewController)
        case .UpdatePasswordView:
            return UpdatePasswordPresenter(wireframe: wireframe, view: viewController)
        case .ProfileView:
            return ProfilePresenter(wireframe: wireframe, view: viewController)
        case .EditProfileView:
            return EditProfilePresenter(wireframe: wireframe, view: viewController)
        }
    }
}


