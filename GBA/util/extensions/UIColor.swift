



import UIKit

extension UIColor{
    
    
    convenience init(hexa: UInt, alpha: CGFloat? = nil){
        let alpha_ = alpha ?? 1
        
        self.init(red: CGFloat((hexa & 0xFF0000) >> 16) / 255,
                  green: CGFloat((hexa & 0x00FF00) >> 8) / 255,
                  blue: CGFloat(hexa & 0x0000FF) / 255,
                  alpha: alpha_)
    }
}
