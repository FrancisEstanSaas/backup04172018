//
//  MapViewController.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 11/3/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GMSServices.provideAPIKey(GBAKeyStrings.GMAP_API)
        
        let camera = GMSCameraPosition.camera(withLatitude: 14.583048, longitude: 121.062352, zoom: 100)
        let mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        
        self.view = mapView
    }
}


extension MapViewController: CLLocationManagerDelegate{
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        if status == .authorizedWhenInUse{
//            locationManager.startUpdatingLocation
//
//        }
//    }
}
