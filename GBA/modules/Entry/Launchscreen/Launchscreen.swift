//
//  ViewController.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/20/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit

class Launchscreen: EntryModuleViewController {
    
    fileprivate var user: UserKeyInfo{
        get{
            guard let usr = GBARealm.objects(UserKeyInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    fileprivate var registeredUser: RegisteredUserInfo{
        get{
            guard let usr = GBARealm.objects(RegisteredUserInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addPrimaryLogInUser()
        self.addRegisteredLogInUser()
        
        guard let appNav = self.navigationController else {
            fatalError("App navigator not found!")
        }
        appNav.isNavigationBarHidden = true
        
        let image = UIImageView()
        image.image = UIImage(imageLiteralResourceName: "logo_white")
        image.contentMode = .scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        
        self.view.addSubview(image)
        
        image.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).Enable()
        image.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).Enable()
        image.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1/2).Enable()
        image.heightAnchor.constraint(equalTo: image.widthAnchor).Enable()
        
        view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let appNav = self.navigationController!
        
        print(UserDefaults.standard.bool(forKey: "isFirstOpened"))
        
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 3, repeats: false, block: {
                _ in
                if UserDefaults.standard.bool(forKey: "isNotFirstOpened"){ self.logInTypeValidator() } //EntryWireframe(appNav).navigate(to: .Loginscreen) }
                else{ EntryWireframe(appNav).navigate(to: .Welcomescreen) }
            })
        } else {

        }
    }
    

//For Alternative LogInVC
    func logInTypeValidator() {

        let appNav = self.navigationController!
        let logInUserProfile = self.registeredUser//should read registered user object from realm.

        if logInUserProfile.logInType == "0" {
            print("Default Log-In Set")
            EntryWireframe(appNav).navigate(to: .Loginscreen)
        } else if logInUserProfile.logInType == "1" {
            print("PIN Code Set")
            EntryWireframe(appNav).navigate(to: .PinCodeLogIn)

        } else if logInUserProfile.logInType == "2"{
            print("Touch ID Log-In Set")
            EntryWireframe(appNav).navigate(to: .TouchIDLogIn)

        } else {
            print("Error in LogIn Type Occured")
            return
        }
    }
   
    //For Primary
    func addPrimaryLogInUser() {
        do {
            if let pinUser = GBARealm.object(ofType: UserKeyInfo.self, forPrimaryKey: "0"){
                //Nothing needs be done. //Primary
                print(pinUser)
                print("PinUser already existing")
            } else {
                let newUser = UserKeyInfo()
                newUser.userNumber = "0"
                newUser.userPassword = "0"
                GBARealm.beginWrite()
                GBARealm.create(UserKeyInfo.self, value: newUser, update: true)
                try GBARealm.commitWrite()
            }
        } catch {
            print("error @addPrimaryLogInUser")
        }
        
    }
    
    //For Primary
    func addRegisteredLogInUser() {
        do {
            if let primaryUser = GBARealm.object(ofType: RegisteredUserInfo.self, forPrimaryKey: "0"){
                //Nothing needs be done. //RegisteredUser
                print(primaryUser)
                print("PinUser already existing")
            } else {
                let newUser = RegisteredUserInfo()
                newUser.userID = "0"
                newUser.logInType = "0"
                GBARealm.beginWrite()
                GBARealm.create(RegisteredUserInfo.self, value: newUser, update: true)
                try GBARealm.commitWrite()
            }
        } catch {
            print("error @addPrimaryLogInUser")
        }
        
    }

    
}

