//
//  GBATitledPopOverButton.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/22/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class GBATitledPopOverButton: GBARootFocusableObject{
    
    private var _title: UILabel = UILabel()
    private var _value_label: UILabel = UILabel()
    
    fileprivate(set) var viewController: UIViewController? = nil
    fileprivate(set) var parent: RootViewController? = nil
    
    fileprivate(set) var title: String = ""{
        didSet{ self._title.text = self.title }
    }
    
    fileprivate(set) var value:String = ""{
        didSet{
            self._value_label.text = self.value
            
            if self.value != ""{
                UIView.animate(withDuration: 0.5, animations: {
                    self._title.frame.origin.y = 0
                })
            }else{
                UIView.animate(withDuration: 0.5, animations: {
                    self._title.center.y = self._value_label.center.y
                })
            }
            self.layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupXib()
        self.isUserInteractionEnabled = true
        let tapped = UITapGestureRecognizer(target: self, action: #selector(openPopupTableview))
        self.addGestureRecognizer(tapped)
    }
    
    private func setupXib(){
        self.backgroundColor = .clear
        let size = self.frame.size
        
        let underline = UIView()
        underline.backgroundColor = GBAColor.lightGray.rawValue
        underline.translatesAutoresizingMaskIntoConstraints = false
        
        let caret = UILabel()
            .add(to: self)
            .set(alignment: .center)
            .set(color: GBAColor.gray.rawValue)
            .set(value: String.fontAwesomeIcon(name: .caretDown))
            .set(fontStyle: UIFont.fontAwesome(ofSize: size.height * 0.12))
        
        self._title
            .add(to: self)
            .set(alignment: .left)
            .set(value: self.title)
            .set(color: GBAColor.gray.rawValue)
            .set(fontStyle: GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue)
            .set(value: self.title)
        
        self._value_label
            .add(to: self)
            .set(alignment: .left)
            .set(value: self.value)
            .set(color: GBAColor.primaryBlueGreen.rawValue)
            .set(fontStyle: GBAText.Font.main(size.height * 0.2).rawValue)
        
        self.addSubview(underline)
        
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -14).Enable()
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        underline.heightAnchor.constraint(equalToConstant: 1).Enable()
        
        _title.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        _title.centerYAnchor.constraint(equalTo: self._value_label.centerYAnchor).Enable()
        _title.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        
        _value_label.leadingAnchor.constraint(equalTo: underline.leadingAnchor).Enable()
        _value_label.trailingAnchor.constraint(equalTo: underline.trailingAnchor).Enable()
        _value_label.bottomAnchor.constraint(equalTo: underline.topAnchor, constant: 5).Enable()
        _value_label.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.6).Enable()
        
        caret.topAnchor.constraint(equalTo: self.topAnchor).Enable()
        caret.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        caret.bottomAnchor.constraint(equalTo: self.bottomAnchor).Enable()
        caret.widthAnchor.constraint(equalTo: self.heightAnchor).Enable()
    }
    
    @objc private func openPopupTableview(){
        guard let parent = self.parent else { fatalError("Popup parent was never sent") }
        guard let vc = viewController else { fatalError("ViewController was not set for PopOverButton") }
        
        vc.modalPresentationStyle = .popover
        
        vc.popoverPresentationController?.permittedArrowDirections = .up
        vc.popoverPresentationController?.delegate = parent
        vc.popoverPresentationController?.sourceView = self
        vc.popoverPresentationController?.sourceRect = self.bounds
        
        parent.view.resignFirstResponder()
        
        parent.present(vc, animated: true, completion: nil)
    }
    
    override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        self.openPopupTableview()
        return true
    }
}

extension GBATitledPopOverButton{
    @discardableResult
    func set(title: String)->Self{
        self.title = title
        return self
    }
    
    @discardableResult
    func set(value: String)->Self{
        self.value = value
        return self
    }
    
    @discardableResult
    func set(parent: RootViewController)->Self{
        self.parent = parent
        return self
    }
    
    @discardableResult
    func set(viewController: UIViewController)->Self{
        self.viewController = viewController
        return self
    }
    
    @discardableResult
    func set()->Self{
        return self
    }
}

