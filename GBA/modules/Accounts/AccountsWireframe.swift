//
//  AccountsWireframe.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class AccountsWireframe: RootWireframe {
    
    func configureModule(activity: AccountsActivities) {
        guard let target = self._target else {
            fatalError("[AccountsWireframe: configureModule] target not found")
        }
        
        if _presenter == nil {
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! AccountsRootViewController, and: self) as? AccountsRootPresenter else {
                fatalError("declared parameter cannot be found in Accounts module")
            }
            
            self.set(presenter: presenter)
        } else {
            guard let presenter = _presenter as? AccountsRootPresenter else {
                fatalError("declared presenter cannot be parsed for Accounts module")
            }
            
            presenter.view = target.module.activity!.viewController as! AccountsRootViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: AccountsActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Accounts(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension AccountsWireframe{
    
    func navigate(to activity: AccountsActivities){
        var transition: Transition = .present
        
        switch activity {
        case .ListGBAWallets:
            transition = .root
        case.ViewAccountDetails:
            transition = .root
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: AccountsActivities)->AccountsRootViewController?{
        self.set(target: .Accounts(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? AccountsRootViewController,
            let presenter = self._presenter else { return nil }
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
}
