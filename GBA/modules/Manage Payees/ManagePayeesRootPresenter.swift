//
//  ManagePayeesPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class ManagePayeesRootPresenter: RootPresenter{
    
    var wireframe: ManagePayeesWireframe
    var view: ManagePayeesModuleViewController
    var interactor: (local: ManagePayeeLocalInteractor, remote: ManagePayeeRemoteInteractor) = (ManagePayeeLocalInteractor(), ManagePayeeRemoteInteractor())
    
    
    func set(view: ManagePayeesModuleViewController){ self.view = view }
    
    init(wireframe: ManagePayeesWireframe, view: ManagePayeesModuleViewController){
        self.wireframe = wireframe
        self.view = view
    }
    
    func showAlert(with title: String?, message: String, completion: @escaping (()->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            alert.dismiss(animated: true, completion: completion)
        }
    }
}
    
